﻿using Newtonsoft.Json;
using StudioKit.Caliper.Exceptions;
using StudioKit.Caliper.Interfaces;
using StudioKit.Encryption;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Caliper;

public class CaliperEventStoreService
{
	private readonly ICaliperConfigurationProvider _configurationProvider;
	private readonly Func<IHttpClient> _getHttpClient;
	private static Dictionary<string, string> _cachedToken;

	public CaliperEventStoreService(ICaliperConfigurationProvider configurationProvider, Func<IHttpClient> getHttpClient)
	{
		_configurationProvider = configurationProvider;
		_getHttpClient = getHttpClient;
	}

	public async Task<Dictionary<string, string>> GetTokenAsync(CancellationToken cancellationToken = default)
	{
		if (_cachedToken != null)
		{
			var didParse = DateTime.TryParse(_cachedToken[".expires"], out var expires);
			// If we have a cached, unexpired token from the caliper service, just return it.
			if (didParse && expires.ToUniversalTime() > DateTime.UtcNow)
				return _cachedToken;
		}

		var configuration = await _configurationProvider.GetCaliperConfigurationAsync(cancellationToken);
		if (configuration == null)
			throw new Exception("Could not load ICaliperConfiguration from ICaliperConfigurationProvider");

		if (!configuration.CaliperEnabled)
			throw new CaliperNotEnabledException();

		var hostname = EncryptedConfigurationManager.TryDecryptSettingValue(configuration.CaliperEventStoreHostname);
		if (string.IsNullOrWhiteSpace(hostname))
			throw new CaliperConfigurationException("ICaliperConfiguration.CaliperEventStoreHostname is required");

		var clientId = EncryptedConfigurationManager.TryDecryptSettingValue(configuration.CaliperEventStoreClientId);
		if (string.IsNullOrWhiteSpace(clientId))
			throw new CaliperConfigurationException("ICaliperConfiguration.CaliperEventStoreClientId is required");

		var clientSecret = EncryptedConfigurationManager.TryDecryptSettingValue(configuration.CaliperEventStoreClientSecret);
		if (string.IsNullOrWhiteSpace(clientId))
			throw new CaliperConfigurationException("ICaliperConfiguration.CaliperEventStoreClientSecret is required");

		var tokenEndpointUri = new Uri($"https://{hostname}{CaliperConstants.OAuthTokenPath}");

		// create client
		var client = _getHttpClient();
		client.Timeout = 300;

		// POST to caliper eventstore
		string responseBody;
		try
		{
			responseBody = await client
				.PostForStringAsync(tokenEndpointUri, _ => new FormUrlEncodedContent(new[]
				{
					new KeyValuePair<string, string>("client_id", clientId),
					new KeyValuePair<string, string>("client_secret", clientSecret),
					new KeyValuePair<string, string>("grant_type", "client_credentials"),
				}), cancellationToken)
				.ConfigureAwait(false);
		}
		catch (HttpRequestException ex)
		{
			throw new CaliperEventStoreRequestException("Caliper EventStore API error", ex);
		}

		// parse json
		var token = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseBody);
		_cachedToken = token;

		return token;
	}
}