﻿namespace StudioKit.Caliper;

public static class CaliperConstants
{
	public const string OAuthTokenPath = "/oauth/token";

	public const string PurduePersonNamespace = "https://purdue.edu/user/";
}