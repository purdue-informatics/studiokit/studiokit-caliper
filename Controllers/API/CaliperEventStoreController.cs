﻿using Microsoft.AspNetCore.Mvc;
using StudioKit.Caliper.Exceptions;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Caliper.Controllers.API;

[Route("api/caliper-eventstore")]
[ApiController]
public class CaliperEventStoreController : ControllerBase
{
	private readonly CaliperEventStoreService _caliperEventStoreService;

	public CaliperEventStoreController(CaliperEventStoreService caliperEventStoreService)
	{
		_caliperEventStoreService = caliperEventStoreService;
	}

	[HttpGet("token")]
	public async Task<IActionResult> GetTokenAsync(CancellationToken cancellationToken)
	{
		try
		{
			var token = await _caliperEventStoreService.GetTokenAsync(cancellationToken);
			return Ok(token);
		}
		catch (CaliperNotEnabledException)
		{
			return Forbid();
		}
	}
}