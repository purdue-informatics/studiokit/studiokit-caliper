﻿using System;

namespace StudioKit.Caliper.Exceptions;

public class CaliperConfigurationException : Exception
{
	public CaliperConfigurationException()
	{
	}

	public CaliperConfigurationException(string message)
		: base(message)
	{
	}

	public CaliperConfigurationException(string message, Exception inner)
		: base(message, inner)
	{
	}
}