﻿using System;

namespace StudioKit.Caliper.Exceptions;

public class CaliperNotEnabledException : Exception
{
	public CaliperNotEnabledException()
	{
	}

	public CaliperNotEnabledException(string message)
		: base(message)
	{
	}

	public CaliperNotEnabledException(string message, Exception inner)
		: base(message, inner)
	{
	}
}